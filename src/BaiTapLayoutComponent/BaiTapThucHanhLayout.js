import React, { Component } from 'react'
import Card from './Card'
import Heading from './Heading'

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
        <>
            <Heading></Heading>
        <section className='pt-4'>
            <div className='container px-lg-5'>
                <div className='row gx-lg-5'>
                    <Card></Card>
                    <Card></Card>
                    <Card></Card>
                    <Card></Card>
                    <Card></Card>
                    <Card></Card>
                </div>
            </div>
        </section>
        </>
    )
  }
}
